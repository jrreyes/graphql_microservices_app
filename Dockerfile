FROM registry.iwa.fi:443/ruby-service-base-alpine

# THIS FILE IS GENERATED FROM Template ./templates/Dockerfile-rails
# You can use it as starting point for your own Dockerfile

# Configure the main working directory. This is the base
# directory used in any further RUN, COPY, and ENTRYPOINT
# commands.
COPY . /app/
WORKDIR /app


# private_key for internal gitlab.iwa.fi
RUN apk update
RUN apk add openssh-client
RUN apk add git
RUN mkdir -p ${HOME}/.ssh
COPY ./id_rsa /root/.ssh/id_rsa
RUN chmod 700 /root/.ssh/id_rsa
RUN echo "    StrictHostKeyChecking no" >> /etc/ssh/ssh_config


RUN bundle install --jobs 20 --retry 5

EXPOSE 3000


# docker inspect --format="{{json .State.Health}}" sakani-projects-graphql
HEALTHCHECK --start-period=30s --retries=10 --timeout=1m CMD \
  cd /app; [ -e dbprovision.completed ] || \
  wait-for $DATABASE_HOST:5432 -- \
  rails db:create RAILS_ENV=development && \
  rails db:migrate RAILS_ENV=development && \
  touch dbprovision.completed


RUN chmod 754 dockercmd.sh
CMD ./dockercmd.sh
