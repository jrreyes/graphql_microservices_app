source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?("/")
  "https://github.com/#{repo_name}.git"
end

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 5.1.4'
# Use postgresql as the database for Active Record
gem 'pg', "~> 0.20"

gem "graphql"
gem "iwa_apina", git: "git@gitlab.iwa.fi:iwa-templates/iwa_apina.git", tag: "1.4.0"
# IWA apina gem for easy GraphQL API creation.

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

# Use Rack CORS for handling Cross-Origin Resource Sharing (CORS), making cross-origin AJAX possible
# gem 'rack-cors'

# Nicely formatted single line logs
gem "lograge"

# Hijri date library for Ruby
gem 'hijri'

gem "figaro"

gem "sakani-constants", git: "git@gitlab.iwa.fi:moho/sakani-constants.git", tag: '0.3.0'

gem 'carrierwave'
gem 'carrierwave-base64', '~> 2.7'


group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: %i[mri mingw x64_mingw]
  gem "factory_bot_rails", "~> 4.0"
  # Use Puma as the app server
  gem 'puma', '~> 3.7'
  gem 'shoulda-matchers', '~> 3.1'
  gem 'graphiql-rails'
  gem 'coffee-rails'
  gem 'sass-rails'
  gem 'uglifier'
  gem 'railroady'

  # For excel rake task
  gem 'rubyXL'
  gem 'sakani-encrypt', git: 'git@gitlab.iwa.fi:moho/sakani-encrypt.git', tag: '0.2.0'

end

group :development do
  gem 'bundler-audit'
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'rspec-rails'
  gem 'rubocop', "~> 0.52.1"
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end

group :production do
  gem 'exception_notification'
  gem 'slack-notifier'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
# gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
# Linux Alpine does not include zoneinfo files
gem 'tzinfo-data'
