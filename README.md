# README

This project provides simple dynamic GraphQL API for basic resource CRUD management. It is based on api-only Rails application and iwa_apina gem. Application stack is tried to keep as lightweight as possible and it is meant to be used in a microservice architecture environment as the lowest level API, between persistent storage and other internal APIs. It does not include layers for business requirements, authentication, authorization or security. Very limited cache is supported.

_Please check `application.rb` and `production.rb` environment for skipped middleware. The current middleware stack in production is as follows:_

```
use ActiveSupport::Cache::Strategy::LocalCache::Middleware
use Rack::Runtime
use ActionDispatch::RequestId
use Rails::Rack::Logger
use ActionDispatch::Callbacks
use Rack::Head
use Rack::ConditionalGet
use Rack::ETag
run SakaniBeneficiariesGraphql::Application.routes
```

**If you are using this project as a template and add some functionality to your project that might benefit this template as well, please remember to share it back! This might be for example updating gems or patching security holes.**


## Ruby version

This project requires at least Ruby version 2.2.2

## System dependencies

This project is tested using PostgreSQL as a database, but with small modifications, it should work with any SQL database.

## Configuration

Copy config/database.yml.sample to config/database.yml
Copy config/secrets.yml.sample to config/secrets.yml
Copy config/cable.yml.sample to config/cable.yml

Configure the above created files accordingly.

In production you also need to setup following environment variables:

```
ENV['exception_ignored_keys'] = "Ignore these keys in exceptions, for example ssn and passwords."
ENV['exception_webhook_url'] = "Webhook url for exceptions to be posted to Slack channel."
ENV["SECRET_KEY_BASE"] = "Secret key base for the rails app, used to hash many things."
```

Setup iwa_apina gem by creating initializer config/initializers/iwa_apina.rb with

```
IwaApina.configure do |config|
  config.api_endpoint = "/unit-api" # API subpath with meaningfull description of the resource
  config.api_types = ['housing_district', 'house', 'location'] # models that are provided through API
end
```

More information how to use iwa_apina, please refer to the project [README](https://gitlab.iwa.fi/iwa-templates/iwa_apina) file.

You can test the API for example with the following POST request
`http://localhost:3000/units-api?query={units(page:{offset:0,limit:10},sort:%20{code:%20%22asc%22}){code,bedroom_count,rooms{purpose,size}}}`

## Database creation

Please run `rake db:setup`

## Database initialization

Please run `rake db:migrate`

## How to run the test suite

This project uses Rspec + FactoryBot for the main means of testing.
Run `rspec rspec` to execute all tests.

## Services (job queues, cache servers, search engines, etc.)

Currently this project does not have any asyncronous services.

## Deployment instructions

To deploy this project, setup config/deploy.rb and config/deploy/[environment].rb and run `cap [environment] deploy`