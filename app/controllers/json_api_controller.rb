class JsonApiController < ActionController::API
  include ActionController::Serialization
  include ActionController::HttpAuthentication::Token::ControllerMethods


  rescue_from ActionController::ParameterMissing,
                with: :bad_request


  protected

  def success_response(data)
    render json: { data: data }
  end

  def unprocessable(resource)
    render json: {
      :message => 'Unprocessable', 
      :errors => resource.errors.full_messages
    }, :status => :unprocessable_entity 
  end

  def no_content
    render json: {:message => 'No content'}, :status => :no_content
  end

  def not_modified_response
    render json: {:message => 'Not modified'}, :status => :not_modified
  end
end
