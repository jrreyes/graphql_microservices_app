# Custom query that locks beneficiary_applications table while creating a booking 
class BeneficiaryApplicationCreateAndLockTable < GraphQL::Function
  # arguments passed as "args"
  argument :beneficiary_national_id_number, !GraphQL::STRING_TYPE
  

  type -> { IwaApina::MY_TYPES["beneficiary_application_type"][:graphqlType] }

  def call(_obj, args, _ctx)
    ActiveRecord::Base.transaction do
      ActiveRecord::Base.connection.execute('LOCK beneficiary_applications IN ACCESS EXCLUSIVE MODE')

      beneficiary_application = BeneficiaryApplication.new(
        beneficiary_national_id_number: args[:beneficiary_national_id_number],
      )
      beneficiary_application.save!

      return beneficiary_application
    end
  end
end
