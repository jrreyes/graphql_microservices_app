class BeneficiaryApplication < ApplicationRecord
  enum employment_level:  SakaniConstants::Beneficiary::EMPLOYMENT_LEVELS
  enum education_level:   SakaniConstants::Beneficiary::EDUCATION_LEVELS
  enum status:            SakaniConstants::BeneficiaryApplication::STATUSES
  enum family_category:   %w(G1 G2 G3 G4 G5 G6 G7 INELIGIBLE)
           # SakaniConstants::BeneficiaryApplication::FAMILY_CATEGORIES
  enum eligible_status:   %w(not_eligible eligible) # TODO: move to SakaniConstants

  has_many :family_members
  has_many :family_real_estates
  has_many :loans
  has_many :supporting_documents

  has_one :family_residency_information
  has_one :financial_information
  has_one :preferred_support

  validates_presence_of :beneficiary_national_id_number
  validate :beneficiary_has_no_active_beneficiary_applications
  before_validation :set_status, :validation_method

  store_accessor :external_validation_errors, :first_choice, :second_choice, :third_choice, :fourth_choice

  scope :active_to_beneficiary, ->(beneficiary_national_id_number){ where(status: ['status_initial', 'status_processing', 'status_approved'], beneficiary_national_id_number: beneficiary_national_id_number) }

  def sent_for_review=(value)
    if value == 1
      self.status = 'status_processing' 
      self.sent_for_review_at = DateTime.now
    end
  end

  def rejected=(value)
    if value == 1
      self.status = 'status_rejected'
      self.rejected_at = DateTime.now
    end
  end

  def approved=(value)
    if value == 1
      self.status = 'status_approved'
      self.approved_at = DateTime.now
    end
  end

  def more_details_needed=(value)
    self.status = 'status_error' if value == 1
  end

  def re_verified=(value)
    self.re_verified_at = DateTime.now if value == 1
  end

  def apply_external_validations
    run_external_validations 
  end

  private

    def set_status
      self.status = 'status_initial' unless self.status.present?
    end

    def run_external_validations
      ['NIC', 'REDF', 'MOJ', 'ElectricityCompany'].each do |api|
        ValidationService.new(self, api).validate
      end
    end

    def validation_method
      self.validate_manually = false
      # if self.orphan_family? || self.expat_single_mother_family?
      #   self.validate_manually = true
      # end
    end

    def beneficiary_has_no_active_beneficiary_applications
      if BeneficiaryApplication.active_to_beneficiary(self.beneficiary_national_id_number).where.not(id: self.id).any?
        errors.add(:beneficiary, "has_already_active_beneficiary_application")
      end
    end
end
