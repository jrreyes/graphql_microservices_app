module GregorianDateFormat
  extend ActiveSupport::Concern

  def is_date_in_gregorian_format?(date_attribute)
    return if self.send(date_attribute.to_sym).blank?

    # TODO fix this duplicate
    gregorian_date =  self[date_attribute.to_sym]

    (Time.now.year - gregorian_date.year) < 100
  end

  def set_date_to_gregorian_format(date_attribute)
    return if self.send(date_attribute.to_sym).blank?

    # TODO fix this duplicate
    gregorian_date =  self[date_attribute.to_sym]

    hijri_date = Hijri::Date.new(gregorian_date.year, gregorian_date.month, gregorian_date.day)
    self.write_attribute(date_attribute.to_sym, hijri_date.to_greo)
  end

  def convert_to_hijri_date(datetime)
    if datetime
      datetime.to_date.to_hijri
    else
      nil
    end
  end

  private

    def format_date_to_gregorian(attribute_name)
      unless self.is_date_in_gregorian_format?(attribute_name)
        self.set_date_to_gregorian_format(attribute_name)
      end
    end
end