class FamilyMember < ApplicationRecord
	include GregorianDateFormat
  
  #enum family_member_type: %w(wife husband son daughter mother father other)

  # SakaniConstants::BeneficiaryApplicationFamilyMember::FAMILY_MEMBER_TYPES

  validates_presence_of :national_id_number, :beneficiary_application

  # validates :previously_received_housing_support, inclusion: [ true, false ]
  # validates :lived_outside_of_ksa, inclusion: [ true, false ]

  belongs_to  :beneficiary_application
  # has_many    :family_real_estates

  before_validation do
    format_date_to_gregorian('date_of_birth')
  end

  def date_of_birth
    convert_to_hijri_date(self[:date_of_birth])
  end
end
