class FamilyRealEstate < ApplicationRecord
  include GregorianDateFormat
  
  enum ownership_status: SakaniConstants::BeneficiaryApplicationFamilyRealEstate::OWNERSHIP_STATUSES
  enum real_estate_type: %w(residential_land commercial_land villa townhouse apartment other housing commercial others agricultural industrial null_from_api)
           # SakaniConstants::BeneficiaryApplicationFamilyRealEstate::REAL_ESTATE_TYPES

  # validates_presence_of :deed_number, :deed_date, :deed_region, :deed_province, :deed_city,
  #                       :electric_bill_number, :family_member, :beneficiary_application

  #before_validation :fetch_beneficiary_application_from_family_member

  #belongs_to :family_member
  belongs_to :beneficiary_application

  before_validation do
    format_date_to_gregorian('deed_date')
  end

  def deed_date
    convert_to_hijri_date(self[:deed_date])
  end

  private

    def fetch_beneficiary_application_from_family_member
      if self.family_member && self.family_member.beneficiary_application_id.present?
        self.beneficiary_application_id = self.family_member.beneficiary_application_id
      end
    end
end
