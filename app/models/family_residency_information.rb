class FamilyResidencyInformation < ApplicationRecord
  enum residency_type: SakaniConstants::BeneficiaryApplicationFamilyResidencyInformation::RESIDENCY_TYPES
  enum residency_unit_type: SakaniConstants::BeneficiaryApplicationFamilyResidencyInformation::RESIDENCY_UNIT_TYPES
  enum residency_status: SakaniConstants::BeneficiaryApplicationFamilyResidencyInformation::RESIDENCY_STATUSES
  
  belongs_to :beneficiary_application
  
  validates_presence_of :residency_type, :beneficiary_application
  validates_uniqueness_of :beneficiary_application_id, message: "has already financial information"
end
