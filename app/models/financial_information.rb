class FinancialInformation < ApplicationRecord
  belongs_to :beneficiary_application
  
  validates_presence_of :beneficiary_application_id
  validates_uniqueness_of :beneficiary_application_id, message: "has already financial information"
end