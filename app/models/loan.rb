class Loan < ApplicationRecord
  enum loan_type: SakaniConstants::BeneficiaryApplicationFinancialInformation::LOAN_TYPES
  
  validates_presence_of :loan_type, :loan_provider_name, :loan_amount, :remaining_loan_amount,
                        :monthly_loan_payment, :loan_reason, :beneficiary_application
  
  belongs_to :beneficiary_application
end
