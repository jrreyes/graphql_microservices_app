class PreferredSupport < ApplicationRecord
  belongs_to :beneficiary_application

  validates_presence_of :beneficiary_application_id, :city, :region, :choice
  validates_uniqueness_of :beneficiary_application, message: "has already preferred support information"

  def first_choice=(value)
    super(value.to_s)
  end

  def second_choice=(value)
    super(value.to_s)
  end

  def third_choice=(value)
    super(value.to_s)
  end

  def fourth_choice=(value)
    super(value.to_s)
  end

  def dublicate_choices?(choice)
    duplicates = false
    
    PreferredSupport.stored_attributes[:choices].each do |p|
      unless choice.to_sym == p
        duplicates = true if self.send(choice.to_sym) == self.send(p)
      end
    end

    duplicates
  end

  private

    def uniqueness_of_choices
      PreferredSupport.stored_attributes[:choices].each do |p|
        if dublicate_choices?(p)
          errors.add(p, "#{self.send(p)} has already taken")
        end
      end
    end
end

