class SupportingDocument < ApplicationRecord
  
  mount_base64_uploader :file, SupportingDocumentUploader

  validates_presence_of :file, :beneficiary_application

  belongs_to :beneficiary_application
end