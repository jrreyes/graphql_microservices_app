class ValidationService

  attr_accessor :validator

  def initialize(beneficiary_application, validation_api)
    @beneficiary_application = beneficiary_application
    @validation_api = validation_api
    @validator = get_validator
  end

  def validate
    
    begin

      @validator.validate
    
      return true

    rescue => ex
      validation_failed_with_message(ex.message)
    end

    false
  end


protected


  def validation_failed_with_message(message, status = :status_error)
    @beneficiary_application.status = status
    @beneficiary_application.external_validation_errors = message
    @beneficiary_application.save
  end

  def get_validator
    case @validation_api
    when "NIC"
      v = Validations::NicApi.new(@beneficiary_application)
    when "REDF"
      v = Validations::RedfApi.new(@beneficiary_application)
    when "MOJ"
      v = Validations::MojApi.new(@beneficiary_application)
    when "ElectricityCompany"
      v = Validations::ElectricityCompanyApi.new(@beneficiary_application)
    end
    v
  end

end