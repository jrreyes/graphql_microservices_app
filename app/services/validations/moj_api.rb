require 'validations/validation_base'

module Validations

  class MojApi < Validations::ValidationBase

    protected

      def validate_beneficiary_application
        applicant_owns_an_appropriate_dwelling?
        family_members_owns_an_appropriate_dwelling?
      end

    private

      def family_members_owns_an_appropriate_dwelling?
        @beneficiary_application.family_members.each do |fm|
          unless [true, false].sample
            @errors.push format_error_message("MOJ | FamilyMember | National ID Number: #{fm.national_id_number}", 'owns an appropriate dwelling')
          end
        end
      end

      def applicant_owns_an_appropriate_dwelling?
        unless [true, false].sample
          @errors.push format_error_message("MOJ |Beneficiary", 'owns an appropriate dwelling')
        end
      end 
      
  end
end