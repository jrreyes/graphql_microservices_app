require 'validations/validation_base'

module Validations

  class NicApi < Validations::ValidationBase

    protected

      def validate_beneficiary_application
        verify_family_structure
        applicant_fills_terms_for_application?
      end

    private

      def verify_family_structure
        @beneficiary_application.family_members.each do |fm|
          if [true, false].sample
            @errors.push format_error_message("NIC | FamilyMember | National ID Number: #{fm.national_id_number}", 'family structure not confirmed')
          end
        end
      end

      def applicant_fills_terms_for_application?
        if [true, false].sample
          @errors.push format_error_message("NIC | Beneficiary", 'does not fill terms for application')
        end
      end 
      
  end
end