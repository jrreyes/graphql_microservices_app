require 'validations/validation_base'

module Validations

  class RedfApi < Validations::ValidationBase

    protected

      def validate_beneficiary_application
        have_previously_received_any_housing_support_from_government?
      end

    private

      def have_previously_received_any_housing_support_from_government?
        if [true, false].sample
          @beneficiary_application.update(dependent_support: true)
          @errors.push format_error_message("MOJ |Beneficiary", 'owns an appropriate dwelling')
        end
      end 
      
  end
end