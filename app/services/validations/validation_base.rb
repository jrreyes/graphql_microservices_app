module Validations

  class ValidationBase

    attr_accessor :beneficiary_application

    def initialize(beneficiary_application)
      @beneficiary_application = beneficiary_application
      initialize_validation
    end

    def validate
      validate_beneficiary_application
      check_errors
    end

    protected

      def validate_beneficiary_application
        raise "Define validate_beneficiary_application in sub class"
      end
      
      def initialize_validation
        raise "Beneficiary Application is missing" unless @beneficiary_application
        
        @national_id_number_of_beneficiary = 'national_id_number_of_beneficiary_123'
        @valid = false
        @errors = []
      end

      def format_error_message(validation_name, message)
        msg = {
          :validation => validation_name,
          :errors => message
        }

        msg
      end

      def check_errors
        if @errors.any?
          @beneficiary_application.update(external_validation_errors: @errors, status: :status_error)
        else
          @beneficiary_application.update(external_validation_errors: nil, status: :status_processing)
        end
      end
  end
end