IwaApina.configure do |config|
  config.api_endpoint = "/beneficiary-applications-api"
  config.api_types = %w[beneficiary_application family_member family_real_estate family_residency_information financial_information loan preferred_support supporting_document]
  config.schema_endpoint = "/beneficiary-applications-schema"

  config.api_virtual_attributes = {
    'PreferredSupport' => {
      first_choice: GraphQL::STRING_TYPE,
      second_choice: GraphQL::STRING_TYPE,
      third_choice: GraphQL::STRING_TYPE,
      fourth_choice: GraphQL::STRING_TYPE,
    }
  }

  config.api_virtual_setters = {
    'PreferredSupport' => {
      first_choice: GraphQL::STRING_TYPE,
      second_choice: GraphQL::STRING_TYPE,
      third_choice: GraphQL::STRING_TYPE,
      fourth_choice: GraphQL::STRING_TYPE,
    },
    'BeneficiaryApplication' => {
      sent_for_review: GraphQL::INT_TYPE,
      rejected: GraphQL::INT_TYPE,
      approved: GraphQL::INT_TYPE,
      more_details_needed: GraphQL::INT_TYPE,
      re_verified: GraphQL::INT_TYPE,
    }
  }

  config.api_custom_queries = {
    'beneficiaryApplicationCreateAndLockTable' => BeneficiaryApplicationCreateAndLockTable,
  }
end