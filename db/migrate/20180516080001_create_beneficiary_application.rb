class CreateBeneficiaryApplication < ActiveRecord::Migration[5.1]
  def change
    create_table :beneficiary_applications do |t|
      t.integer :employment_level
      t.boolean :applicant_lived_outside_ksa
      t.text :applicant_lived_outside_ksa_additional_information
      t.boolean :dependent_support
      t.boolean :dependent_free_land
      t.boolean :have_special_needs
      t.boolean :chronic_diseases
      t.integer :family_category
      t.integer :education_level
      t.integer :beneficiary_id
      t.timestamps
    end
  end
end
