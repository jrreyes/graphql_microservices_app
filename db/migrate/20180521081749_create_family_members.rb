class CreateFamilyMembers < ActiveRecord::Migration[5.1]
  def change
    create_table :family_members do |t|
    	t.integer  		:family_member_type,    null: false
    	t.string 		:national_id_number
    	t.string 		:national_id_number_type
    	t.datetime  	:date_of_birth
    	t.boolean 		:previously_received_housing_support, default: false
			t.boolean			:special_needs_or_chronic_diseases, default: false
    	t.integer 		:number_of_days_outside_of_ksa
    	t.references 	:beneficiary_application, index: { name: 'index_family_members_on_beneficiary_application' }
    	
    	t.timestamps
    end
  end
end
