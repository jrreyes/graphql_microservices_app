class CreateFamilyRealEstates < ActiveRecord::Migration[5.1]
  def change
    create_table :family_real_estates do |t|
      t.integer     :ownership_status,    null: false
      t.integer     :real_estate_type,    null: false
      t.float       :size,                default: 0.0
      t.string      :deed_number
      t.datetime    :deed_date
      t.string      :deed_region
      t.string      :deed_province
      t.string      :deed_city
      t.string      :electric_bill_number
      t.string      :water_bill_number
      t.float       :lat
      t.float       :lng
      
      t.references  :beneficiary_application, index: { name: 'index_family_real_estates_on_beneficiary_application' }
      t.references  :family_member, index: { name: 'index_family_real_estates_on_family_member' }
      
      t.timestamps
    end
  end
end
