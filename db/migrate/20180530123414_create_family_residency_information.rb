class CreateFamilyResidencyInformation < ActiveRecord::Migration[5.1]
  def change
    create_table :family_residency_informations do |t|
      t.integer     :residency_type,          null: false
      t.integer     :residency_unit_type    
      t.integer     :residency_status
      t.float       :rental_price,            default: 0.0
      t.string      :owner_first_name
      t.string      :owner_middle_name
      t.string      :owner_last_name
      t.string      :electric_bill_number
      t.string      :water_bill_number
      t.float       :lat
      t.float       :lng
      
      t.belongs_to  :beneficiary_application, index: { name: 'index_family_residency_informations_on_beneficiary_application' }
      
      t.timestamps
    end
  end
end
