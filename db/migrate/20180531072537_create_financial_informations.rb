class CreateFinancialInformations < ActiveRecord::Migration[5.1]
  def change
    create_table :financial_informations do |t|
      t.float       :family_monthly_salary,           default: 0.0
      t.float       :family_other_income,             default: 0.0
      t.integer     :amount_of_down_payment
      t.float       :costs,                           default: 0.0
      t.text        :cost_description

      t.belongs_to  :beneficiary_application
      
      t.timestamps
    end
  end
end
