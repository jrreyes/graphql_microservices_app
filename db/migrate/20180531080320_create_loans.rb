class CreateLoans < ActiveRecord::Migration[5.1]
  def change
    create_table :loans do |t|
      t.integer     :loan_type
      t.string      :loan_provider_name
      t.float       :loan_amount,                     default: 0.0
      t.float       :remaining_loan_amount,           default: 0.0
      t.float       :monthly_loan_payment,            default: 0.0
      t.text        :loan_reason

      t.belongs_to  :beneficiary_application
      
      t.timestamps
    end
  end
end
