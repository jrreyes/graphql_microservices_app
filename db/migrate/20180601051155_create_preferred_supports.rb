class CreatePreferredSupports < ActiveRecord::Migration[5.1]
  def change
    create_table :preferred_supports do |t|
      t.string        :region
      t.string        :city

      t.belongs_to  :beneficiary_application
      
      t.timestamps
    end
  end
end
