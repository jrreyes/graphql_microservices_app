class AddChoicesToPreferredSupports < ActiveRecord::Migration[5.1]
  def change
    enable_extension "hstore"
    add_column :preferred_supports, :choices, :hstore
    add_index :preferred_supports, :choices, using: :gin
  end
end
