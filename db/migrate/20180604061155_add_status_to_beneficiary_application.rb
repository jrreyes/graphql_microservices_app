class AddStatusToBeneficiaryApplication < ActiveRecord::Migration[5.1]
  def change
    add_column :beneficiary_applications, :status, :integer, null: false
  end
end
