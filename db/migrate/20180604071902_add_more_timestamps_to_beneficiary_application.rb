class AddMoreTimestampsToBeneficiaryApplication < ActiveRecord::Migration[5.1]
  def change
    add_column :beneficiary_applications, :sent_for_review_at,  :datetime
    add_column :beneficiary_applications, :rejected_at,         :datetime
    add_column :beneficiary_applications, :approved_at,         :datetime
    add_column :beneficiary_applications, :re_verified_at,      :datetime
  end
end
