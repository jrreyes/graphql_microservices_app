class AddExternalValidationErrorsToBeneficiariyApplications < ActiveRecord::Migration[5.1]
  def change
    add_column :beneficiary_applications, :external_validation_errors, :text
  end
end
