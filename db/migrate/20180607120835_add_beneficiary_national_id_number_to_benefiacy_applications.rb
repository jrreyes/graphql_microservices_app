class AddBeneficiaryNationalIdNumberToBenefiacyApplications < ActiveRecord::Migration[5.1]
  def change
    add_column :beneficiary_applications, :beneficiary_national_id_number, :string, null: false
    add_index  :beneficiary_applications, :beneficiary_national_id_number, name: 'index_applications_on_beneficiary_national_id_number'
  end
end
