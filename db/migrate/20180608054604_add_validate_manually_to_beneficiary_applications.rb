class AddValidateManuallyToBeneficiaryApplications < ActiveRecord::Migration[5.1]
  def change
    add_column :beneficiary_applications, :validate_manually, :boolean, default: false
  end
end
