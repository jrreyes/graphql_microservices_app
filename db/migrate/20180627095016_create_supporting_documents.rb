class CreateSupportingDocuments < ActiveRecord::Migration[5.1]
  def change
    create_table :supporting_documents do |t|
      t.string :file
      t.text :description

      t.belongs_to  :beneficiary_application
      
      t.timestamps
    end
  end
end
