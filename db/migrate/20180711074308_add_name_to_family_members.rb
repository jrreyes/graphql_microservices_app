class AddNameToFamilyMembers < ActiveRecord::Migration[5.1]
  def change
    add_column :family_members, :name, :string
  end
end
