class RenameFamilyMemberKsa < ActiveRecord::Migration[5.1]
  def change
    rename_column :family_members, :number_of_days_outside_of_ksa, :lived_outside_of_ksa
    change_column :family_members, :lived_outside_of_ksa, 'boolean USING CAST(lived_outside_of_ksa AS boolean)'
  end
end
