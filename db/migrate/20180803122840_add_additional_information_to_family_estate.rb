class AddAdditionalInformationToFamilyEstate < ActiveRecord::Migration[5.1]
  def change
    add_column :family_real_estates, :additional_information, :text
  end
end
