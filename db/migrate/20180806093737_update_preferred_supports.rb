class UpdatePreferredSupports < ActiveRecord::Migration[5.1]
  def change
    remove_column :preferred_supports, :choices
    add_column :preferred_supports, :choice, :string
    add_column :preferred_supports, :bank_cr_number, :string
  end
end
