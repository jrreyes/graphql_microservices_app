class AddResidencyTypes < ActiveRecord::Migration[5.1]
  def change
    add_column :beneficiary_applications, :residency_type, :string
    add_column :beneficiary_applications, :residency_unit_type, :string
    add_column :beneficiary_applications, :lat, :float
    add_column :beneficiary_applications, :lng, :float
  end
end
