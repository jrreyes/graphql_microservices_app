class UpdateFields < ActiveRecord::Migration[5.1]
  # NHC-159
  def change
    # Add description field if user selects 'other'
    add_column :beneficiary_applications, :residency_type_other_details, :string

    # Remove coordinates for map
    remove_column :beneficiary_applications, :lat
    remove_column :beneficiary_applications, :lng

    # Remove national id type
    remove_column :family_members, :national_id_number_type

    # Remove location from real estate
    remove_column :family_real_estates, :lat
    remove_column :family_real_estates, :lng

    # Values from api flags
    add_column :beneficiary_applications, :values_from_api, :integer
    add_column :family_members, :values_from_api, :integer
    add_column :family_real_estates, :values_from_api, :integer
    add_column :family_residency_informations, :values_from_api, :integer
    add_column :financial_informations, :values_from_api, :integer
    add_column :loans, :values_from_api, :integer
    add_column :preferred_supports, :values_from_api, :integer
  end
end
