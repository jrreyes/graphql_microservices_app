class AddEligibleStatus < ActiveRecord::Migration[5.1]
  def change
    add_column :beneficiary_applications, :eligible_status, :integer
  end
end
