class AddNotEligibleValue < ActiveRecord::Migration[5.1]
  def change
    # Not eligible values
    add_column :beneficiary_applications, :not_eligible_values, :integer
    add_column :family_members, :not_eligible_values, :integer
    add_column :family_real_estates, :not_eligible_values, :integer
    add_column :family_residency_informations, :not_eligible_values, :integer
    add_column :financial_informations, :not_eligible_values, :integer
    add_column :loans, :not_eligible_values, :integer
    add_column :preferred_supports, :not_eligible_values, :integer
  end
end
