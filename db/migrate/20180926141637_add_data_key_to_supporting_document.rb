class AddDataKeyToSupportingDocument < ActiveRecord::Migration[5.1]
  def change
    add_column :supporting_documents, :data_key, :string
    add_column :supporting_documents, :data_table_name, :string
    add_column :supporting_documents, :data_object_id, :string
  end
end
