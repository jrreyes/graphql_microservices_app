class AddRedfStatus < ActiveRecord::Migration[5.1]
  def change
    add_column :beneficiary_applications, :redf_status, :integer, default: 0
    add_column :beneficiary_applications, :has_grant, :boolean, default: false
  end
end
