class AllowNullableInRealEstate < ActiveRecord::Migration[5.1]
  def change
    change_column :family_real_estates, :ownership_status, :integer, :null => true
    change_column :family_real_estates, :real_estate_type, :integer, :null => true

  end
end
