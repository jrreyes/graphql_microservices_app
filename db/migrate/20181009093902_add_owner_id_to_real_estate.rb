class AddOwnerIdToRealEstate < ActiveRecord::Migration[5.1]
  def change
    add_column :family_real_estates, :owner_id, :string, index: true
  end
end
