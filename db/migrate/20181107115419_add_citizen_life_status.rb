class AddCitizenLifeStatus < ActiveRecord::Migration[5.1]
  def change
    add_column :beneficiary_applications, :citizen_life_status, :boolean
    add_column :beneficiary_applications, :marital_status, :string
  end
end
