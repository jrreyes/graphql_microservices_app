class AddMombraLicenceFields < ActiveRecord::Migration[5.1]
  def change
      add_column :family_real_estates, :building_licence_type, :string
      add_column :family_real_estates, :building_type, :string
  end
end
