class ChangeFamilyMemberType < ActiveRecord::Migration[5.1]
  def change
    change_column :family_members, :family_member_type, :string #'boolean USING CAST(lived_outside_of_ksa AS boolean)'
  end
end
