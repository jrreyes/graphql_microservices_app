class AddIndigentValue < ActiveRecord::Migration[5.1]
  def change
    add_column :beneficiary_applications, :indigent_value, :float
  end
end
