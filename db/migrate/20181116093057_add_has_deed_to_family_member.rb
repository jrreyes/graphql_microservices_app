class AddHasDeedToFamilyMember < ActiveRecord::Migration[5.1]
  def change
    add_column :family_members, :has_deeds_or_building_licenses, :boolean, default: false
  end
end
