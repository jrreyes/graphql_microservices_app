class AddJobDataToFinancialInformation < ActiveRecord::Migration[5.1]
  def change
    add_column :financial_informations, :job_step, :integer
    add_column :financial_informations, :job_grade, :integer
    add_column :financial_informations, :job_salary, :integer
  end
end
