# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20181130145854) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "hstore"

  create_table "beneficiary_applications", force: :cascade do |t|
    t.integer "employment_level"
    t.boolean "applicant_lived_outside_ksa"
    t.text "applicant_lived_outside_ksa_additional_information"
    t.boolean "dependent_support"
    t.boolean "dependent_free_land"
    t.boolean "have_special_needs"
    t.boolean "chronic_diseases"
    t.integer "family_category"
    t.integer "education_level"
    t.integer "beneficiary_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "status", null: false
    t.datetime "sent_for_review_at"
    t.datetime "rejected_at"
    t.datetime "approved_at"
    t.datetime "re_verified_at"
    t.text "external_validation_errors"
    t.string "beneficiary_national_id_number", null: false
    t.boolean "validate_manually", default: false
    t.string "residency_type"
    t.string "residency_unit_type"
    t.string "residency_type_other_details"
    t.integer "values_from_api"
    t.integer "eligible_status"
    t.integer "not_eligible_values"
    t.integer "redf_status", default: 0
    t.boolean "has_grant", default: false
    t.boolean "citizen_life_status"
    t.string "marital_status"
    t.float "indigent_value"
    t.index ["beneficiary_national_id_number"], name: "index_applications_on_beneficiary_national_id_number"
  end

  create_table "family_members", force: :cascade do |t|
    t.string "family_member_type", null: false
    t.string "national_id_number"
    t.datetime "date_of_birth"
    t.boolean "previously_received_housing_support", default: false
    t.boolean "special_needs_or_chronic_diseases", default: false
    t.boolean "lived_outside_of_ksa"
    t.bigint "beneficiary_application_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name"
    t.integer "values_from_api"
    t.integer "not_eligible_values"
    t.boolean "has_deeds_or_building_licenses", default: false
    t.index ["beneficiary_application_id"], name: "index_family_members_on_beneficiary_application"
  end

  create_table "family_real_estates", force: :cascade do |t|
    t.integer "ownership_status"
    t.integer "real_estate_type"
    t.float "size", default: 0.0
    t.string "deed_number"
    t.datetime "deed_date"
    t.string "deed_region"
    t.string "deed_province"
    t.string "deed_city"
    t.string "electric_bill_number"
    t.string "water_bill_number"
    t.bigint "beneficiary_application_id"
    t.bigint "family_member_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "additional_information"
    t.integer "values_from_api"
    t.integer "not_eligible_values"
    t.string "owner_id"
    t.string "building_licence_type"
    t.string "building_type"
    t.integer "owning_percentage"
    t.index ["beneficiary_application_id"], name: "index_family_real_estates_on_beneficiary_application"
    t.index ["family_member_id"], name: "index_family_real_estates_on_family_member"
  end

  create_table "family_residency_informations", force: :cascade do |t|
    t.integer "residency_type", null: false
    t.integer "residency_unit_type"
    t.integer "residency_status"
    t.float "rental_price", default: 0.0
    t.string "owner_first_name"
    t.string "owner_middle_name"
    t.string "owner_last_name"
    t.string "electric_bill_number"
    t.string "water_bill_number"
    t.float "lat"
    t.float "lng"
    t.bigint "beneficiary_application_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "values_from_api"
    t.integer "not_eligible_values"
    t.index ["beneficiary_application_id"], name: "index_family_residency_informations_on_beneficiary_application"
  end

  create_table "financial_informations", force: :cascade do |t|
    t.float "family_monthly_salary", default: 0.0
    t.float "family_other_income", default: 0.0
    t.integer "amount_of_down_payment"
    t.float "costs", default: 0.0
    t.text "cost_description"
    t.bigint "beneficiary_application_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "values_from_api"
    t.integer "not_eligible_values"
    t.integer "job_step"
    t.integer "job_grade"
    t.integer "job_salary"
    t.index ["beneficiary_application_id"], name: "index_financial_informations_on_beneficiary_application_id"
  end

  create_table "loans", force: :cascade do |t|
    t.integer "loan_type"
    t.string "loan_provider_name"
    t.float "loan_amount", default: 0.0
    t.float "remaining_loan_amount", default: 0.0
    t.float "monthly_loan_payment", default: 0.0
    t.text "loan_reason"
    t.bigint "beneficiary_application_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "values_from_api"
    t.integer "not_eligible_values"
    t.index ["beneficiary_application_id"], name: "index_loans_on_beneficiary_application_id"
  end

  create_table "preferred_supports", force: :cascade do |t|
    t.string "region"
    t.string "city"
    t.bigint "beneficiary_application_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "choice"
    t.string "bank_cr_number"
    t.integer "values_from_api"
    t.integer "not_eligible_values"
    t.index ["beneficiary_application_id"], name: "index_preferred_supports_on_beneficiary_application_id"
  end

  create_table "supporting_documents", force: :cascade do |t|
    t.string "file"
    t.text "description"
    t.bigint "beneficiary_application_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "data_key"
    t.string "data_table_name"
    t.string "data_object_id"
    t.index ["beneficiary_application_id"], name: "index_supporting_documents_on_beneficiary_application_id"
  end

end
