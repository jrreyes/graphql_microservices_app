#!/bin/sh
#substitute common variables with specific ones
if [ $DATABASE_NAME__SAKANI_BENEFICIARIES ];
  then export DATABASE_NAME=$DATABASE_NAME__SAKANI_BENEFICIARIES;
fi
rm -f tmp/pids/server.pid
bundle exec rails s -p 3000 -b '0.0.0.0'
