require 'sakani_encrypt'
require 'rubyXL'

task :parse_excel, [:file_name] => :environment do |task, args|
  # Check arguments
  if args.file_name.nil?
    puts 'Wrong arguments!'
    next
  end
  file_name = args.file_name

  workbook = RubyXL::Parser.parse(file_name)
  worksheet = workbook[0]
  rows = worksheet[3..4]

  rows.each_with_index do |row, index|
    index_in_excel = index + 3
    encrypted_nin = SakaniEncrypt.encrypt(row.cells[0].value.to_s)
    application = BeneficiaryApplication.find_by(beneficiary_national_id_number: encrypted_nin)

    if !application.nil?
      # LIFE STATUS
      parse_cell_content(row, worksheet, index_in_excel, 2, application.citizen_life_status)
      # MARITAL STATUS
      parse_cell_content(row, worksheet, index_in_excel, 3, application.marital_status)

      # DEPENDENTS
      loop_dependents(application, row, worksheet, index_in_excel)

      # REDF PERSON INFO
      parse_cell_content(row, worksheet, index_in_excel, 14, application.redf_status)

      # Realestate type
      loop_realestates(application, row, worksheet, index_in_excel, 15)

      # Has grant
      parse_cell_content(row, worksheet, index_in_excel, 18, application.has_grant ? 'YES' : 'NO')

      # Pension - Family month salary
      parse_financial_information(application, row, worksheet, index_in_excel, 24)

      # Eligible status
      parse_cell_content(row, worksheet, index_in_excel, 26, application.eligible_status.to_s)
    end


  end

  workbook.write("file.xlsx")
end

def parse_cell_content(row, worksheet, index_in_excel, col_index, new_value)
  if row.cells[col_index]
    row.cells[col_index].change_contents(new_value)
  else
    worksheet.add_cell(index_in_excel, col_index, new_value)
  end
end

def loop_dependents(application, row, worksheet, index_in_excel)
  application.family_members.each_with_index do |family_member, family_index|
    break if family_index > 5
    col_index1 = 4 + (family_index * 2)
    col_index2 = 5 + (family_index * 2)
    # puts "col index #{col_index1} #{col_index2} index in excel #{index_in_excel}"
    parse_cell_content(row, worksheet, index_in_excel, col_index1, family_member.national_id_number)
    parse_cell_content(row, worksheet, index_in_excel, col_index2, family_member.family_member_type)
  end
end

def loop_realestates(application, row, worksheet, index_in_excel, col_index)
  real_estate = application.family_real_estates.first
  if real_estate
    parse_cell_content(row, worksheet, index_in_excel, col_index, real_estate.real_estate_type)
  end
end

def parse_financial_information(application, row, worksheet, index_in_excel, col_index)
  finan_info = application.financial_information
  if finan_info
    parse_cell_content(row, worksheet, index_in_excel, col_index, finan_info.family_monthly_salary)
  end
end
