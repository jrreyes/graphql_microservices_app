FactoryBot.define do
  factory :beneficiary_application do
    id 1
    employment_level SakaniConstants::Beneficiary::EMPLOYMENT_LEVELS.sample
    beneficiary_national_id_number SecureRandom.hex(24)
    applicant_lived_outside_ksa false
    dependent_support false
    dependent_free_land false
    have_special_needs false
    chronic_diseases false
    family_category SakaniConstants::BeneficiaryApplication::FAMILY_CATEGORIES.sample
    education_level SakaniConstants::Beneficiary::EDUCATION_LEVELS.sample
    status 'status_initial'
    sent_for_review_at nil
    rejected_at nil
    approved_at nil
    re_verified_at nil
    validate_manually false
  end
end
