FactoryBot.define do
  factory :family_member do
    family_member_type SakaniConstants::BeneficiaryApplicationFamilyMember::FAMILY_MEMBER_TYPES.sample
    national_id_number { rand(100..1000000) }
    national_id_number_type SakaniConstants::BeneficiaryApplicationFamilyMember::NATIONAL_ID_NUMBER_TYPES.sample
    date_of_birth Date.current
    previously_received_housing_support false
    number_of_days_outside_of_ksa 0
    beneficiary_application_id 1
  end
end
