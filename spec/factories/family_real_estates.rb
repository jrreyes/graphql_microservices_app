FactoryBot.define do
  factory :family_real_estate do
    ownership_status SakaniConstants::BeneficiaryApplicationFamilyRealEstate::OWNERSHIP_STATUSES.sample
    real_estate_type SakaniConstants::BeneficiaryApplicationFamilyRealEstate::REAL_ESTATE_TYPES.sample
    size { rand(100..1000000) }
    deed_number "DEED_NUMBER_123"
    deed_date Date.current
    deed_region "Deed Region"
    deed_province "Deed Province"
    deed_city "Deed City"
    electric_bill_number "ELECTRIC_BILL_NUMBER_123"
    water_bill_number "WATER_BILL_NUMBER_123"
    lat { rand(20.0..30.0) }
    lng { rand(40.0..50.0) }
    beneficiary_application nil
    family_member nil
  end
end
