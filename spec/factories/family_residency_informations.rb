FactoryBot.define do
  factory :family_residency_information do
    residency_type SakaniConstants::BeneficiaryApplicationFamilyResidencyInformation::RESIDENCY_TYPES.sample
    residency_unit_type SakaniConstants::BeneficiaryApplicationFamilyResidencyInformation::RESIDENCY_UNIT_TYPES.sample
    residency_status SakaniConstants::BeneficiaryApplicationFamilyResidencyInformation::RESIDENCY_STATUSES.sample
    rental_price { rand(100.0..1000000.0) }
    owner_first_name "FirstName"
    owner_middle_name "MiddleName"
    owner_last_name "LastName"
    electric_bill_number "ELECTRIC_BILL_NUMBER_123"
    water_bill_number "WATER_BILL_NUMBER_123"
    lat { rand(20.0..30.0) }
    lng { rand(40.0..50.0) }
    beneficiary_application nil
  end
end