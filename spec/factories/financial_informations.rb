FactoryBot.define do
  factory :financial_information do
    family_monthly_salary { rand(0.0..1000000.0) }
    family_other_income { rand(0.0..1000000.0) }
    amount_of_down_payment { rand(0.0..1000000.0) }
    costs { rand(0.0..1000000.0) }
    cost_description "Costs description"
    beneficiary_application nil
  end
end