FactoryBot.define do
  factory :loan do
    loan_type SakaniConstants::BeneficiaryApplicationFinancialInformation::LOAN_TYPES.sample
    loan_provider_name "Loan provider name"
    loan_amount { rand(0.0..1000000.0) }
    remaining_loan_amount { rand(0.0..1000000.0) }
    monthly_loan_payment { rand(0.0..1000000.0) }
    loan_reason "Loan reason"
    beneficiary_application nil
  end
end