FactoryBot.define do
  factory :preferred_support do
    region "Region"
    city "City"
    first_choice 'villa'
    second_choice 'apartment'
    third_choice 'townhouse'
    fourth_choice 'land'
    beneficiary_application nil
  end
end