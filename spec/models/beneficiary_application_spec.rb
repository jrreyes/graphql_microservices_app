require 'rails_helper'

RSpec.describe BeneficiaryApplication, type: :model do
    describe "validations" do
        
        subject { build :beneficiary_application }  

        context "Have valid factory" do
            it { should be_valid }
        end
        context "have many" do
            it { should have_many(:family_real_estates) }
            it { should have_many(:family_members) }
            it { should have_many(:loans) }
            it { should have_many(:supporting_documents) }
        end
        context "presence of" do 
            it { should validate_presence_of(:beneficiary_national_id_number) } 
        end
        context "have one" do
            it { should have_one(:family_residency_information) }
            it { should have_one(:financial_information) }
            it { should have_one(:preferred_support) }
        end
        context "enum values" do
            it { should define_enum_for(:employment_level).with(SakaniConstants::Beneficiary::EMPLOYMENT_LEVELS) }
            it { should define_enum_for(:education_level).with(SakaniConstants::Beneficiary::EDUCATION_LEVELS) }
            it { should define_enum_for(:status).with(SakaniConstants::BeneficiaryApplication::STATUSES) }
            it { should define_enum_for(:family_category).with(SakaniConstants::BeneficiaryApplication::FAMILY_CATEGORIES) }
        end
        context "Have a status" do
            
            describe "status is nil" do
                it "have a default status" do
                    subject.status = nil
                    expect(subject).to be_valid
                    expect(subject.status).to eq('status_initial')
                end
            end

            describe "status is set on init" do
                it "is same as initialized" do
                    subject.status = 'status_processing'
                    expect(subject).to be_valid
                    expect(subject.status).to eq('status_processing')
                end
            end

        end
        context "validate_manually" do
            
            describe "family_category is orphan_family" do
                
                it "validates manually" do
                    subject.orphan_family!
                    expect(subject.validate_manually).to be_truthy
                end
            end

            describe "family_category is expat_single_mother_family" do
                
                it "validates manually" do
                    subject.expat_single_mother_family!
                    expect(subject.validate_manually).to be_truthy
                end
            end
   
            describe "family_category is regular_household" do
                
                it "validates automatically" do
                    subject.regular_household!
                    expect(subject.validate_manually).to be_falsey
                end
            end

            describe "family_category is regular_household_where_wife_as_provider" do
                
                it "validates automatically" do
                    subject.regular_household_where_wife_as_provider!
                    expect(subject.validate_manually).to be_falsey
                end
            end

            describe "family_category is single_father" do
                
                it "validates automatically" do
                    subject.single_father!
                    expect(subject.validate_manually).to be_falsey
                end
            end

            describe "family_category is single_mother_as_a_provider" do
                
                it "validates automatically" do
                    subject.single_mother_as_a_provider!
                    expect(subject.validate_manually).to be_falsey
                end
            end
        end
    end
    describe "Virtual setters" do
        
        subject { build :beneficiary_application }

        context "Statuses" do
            
            describe "sent_for_review" do
                it "sets correct status and timestamp" do
                    expect(subject.sent_for_review_at).to be_nil
                    subject.sent_for_review = 1
                    
                    expect(subject.status).to eq('status_processing')
                    expect(subject.sent_for_review_at).not_to be_nil
                end

                it "doesnt change status and timestamp without correct value" do
                    expect(subject.sent_for_review_at).to be_nil
                    subject.sent_for_review = 0
                    
                    expect(subject.status).to eq('status_initial')
                    expect(subject.sent_for_review_at).to be_nil
                end
            end

            describe "rejected" do
                it "sets correct status and timestamp" do
                    expect(subject.rejected_at).to be_nil
                    subject.rejected = 1
                    
                    expect(subject.status).to eq('status_rejected')
                    expect(subject.rejected_at).not_to be_nil
                end

                it "doesnt change status and timestamp without correct value" do
                    expect(subject.rejected_at).to be_nil
                    subject.rejected = 0
                    
                    expect(subject.status).to eq('status_initial')
                    expect(subject.rejected_at).to be_nil
                end
            end

            describe "approved" do
                it "sets correct status and timestamp" do
                    expect(subject.approved_at).to be_nil
                    subject.approved = 1
                    
                    expect(subject.status).to eq('status_approved')
                    expect(subject.approved_at).not_to be_nil
                end

                it "doesnt change status and timestamp without correct value" do
                    expect(subject.approved_at).to be_nil
                    subject.approved = 0
                    
                    expect(subject.status).to eq('status_initial')
                    expect(subject.approved_at).to be_nil
                end
            end

            describe "more_details_needed" do
                it "sets correct status" do
                    subject.more_details_needed = 1
                    
                    expect(subject.status).to eq('status_error')
                end

                it "doesnt change status without correct value" do
                    subject.more_details_needed = 0
                    
                    expect(subject.status).to eq('status_initial')
                end
            end

            describe "re_verified" do
                it "sets timestamp" do
                    expect(subject.re_verified_at).to be_nil
                    subject.re_verified = 1
                    
                    expect(subject.re_verified_at).not_to be_nil
                end

                it "doesnt change timestamp without correct value" do
                    expect(subject.re_verified_at).to be_nil
                    subject.re_verified = 0
                    
                    expect(subject.re_verified_at).to be_nil
                end
            end
        end
    end
end
