require 'rails_helper'

RSpec.describe FamilyMember, type: :model do
    describe "validations" do
        let(:beneficiary_application) { create(:beneficiary_application) }  
        subject { build :family_member, beneficiary_application: beneficiary_application }  

        context "Have valid factory" do
            it { should be_valid }
        end
        context "presence of" do 
            it { should validate_presence_of(:national_id_number) } 
            it { should validate_presence_of(:national_id_number_type) } 
            it { should validate_presence_of(:date_of_birth) }
            it { should validate_presence_of(:number_of_days_outside_of_ksa) }
        end
        context "enum values" do
            it { should define_enum_for(:family_member_type).with(SakaniConstants::BeneficiaryApplicationFamilyMember::FAMILY_MEMBER_TYPES) }
        end
        context "inclusion of" do
            it { should validate_inclusion_of(:national_id_number_type).in_array(SakaniConstants::BeneficiaryApplicationFamilyMember::NATIONAL_ID_NUMBER_TYPES) }
            it { should validate_inclusion_of(:previously_received_housing_support).in_array([true, false]) }
        end
        context "belong to" do
            it { should belong_to(:beneficiary_application) }
        end
        context "have many" do
            it { should have_many(:family_real_estates) }
        end
    end

    describe "gregorian date functions" do 
        context "check date_of_birth gragorian date validations" do
            it "should return nil if date_of_birth is nil" do
                subject.date_of_birth = nil
                subject.valid?
                expect(subject.date_of_birth).to be_nil
            end
            it "shouldn't change as gregorian formated already" do
                subject.date_of_birth = Date.current
                subject.valid?
                expect(subject.date_of_birth).to eq(Date.current)
            end
            it "should return false if date_of_birth isn't in gragorian format" do
                subject.date_of_birth = Date.current - 200.years
                expect(subject.is_date_in_gregorian_format?('date_of_birth')).to be_falsey
            end
            it "should change to gragorian format" do
                subject.date_of_birth = Date.current - 200.years
                subject.valid?
                expect(subject.is_date_in_gregorian_format?('date_of_birth')).to be_truthy
            end
        end
    end

end
