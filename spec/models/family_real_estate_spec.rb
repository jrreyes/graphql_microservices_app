require 'rails_helper'

RSpec.describe FamilyRealEstate, type: :model do
    describe "validations" do
        let(:beneficiary_application) { create(:beneficiary_application) }
        let(:family_member) { create(:family_member, beneficiary_application: beneficiary_application) }  
        
        subject { build :family_real_estate, family_member: family_member, beneficiary_application: beneficiary_application }  

        context "Have valid factory" do
            it { should be_valid }
        end
        context "presence of" do 
            it { should validate_presence_of(:deed_number) } 
            it { should validate_presence_of(:deed_date) } 
            it { should validate_presence_of(:deed_region) }
            it { should validate_presence_of(:deed_province) }
            it { should validate_presence_of(:deed_city) }
            it { should validate_presence_of(:electric_bill_number) }
            it { should validate_presence_of(:lat) }
            it { should validate_presence_of(:lng) }
            it { should validate_presence_of(:family_member) }
        end
        context "enum values" do
            it { should define_enum_for(:ownership_status).with(SakaniConstants::BeneficiaryApplicationFamilyRealEstate::OWNERSHIP_STATUSES) }
            it { should define_enum_for(:real_estate_type).with(SakaniConstants::BeneficiaryApplicationFamilyRealEstate::REAL_ESTATE_TYPES) }
        end
        context "belong to" do
            it { should belong_to(:beneficiary_application) }
            it { should belong_to(:family_member) }
        end
    end

end
