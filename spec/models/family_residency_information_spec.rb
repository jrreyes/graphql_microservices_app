require 'rails_helper'

RSpec.describe FamilyResidencyInformation, type: :model do
    describe "validations" do
        let(:beneficiary_application) { create(:beneficiary_application) }
        
        subject { build :family_residency_information, beneficiary_application: beneficiary_application }  

        context "Have valid factory" do
            it { should be_valid }
        end
        context "presence of" do 
            it { should validate_presence_of(:residency_type) } 
        end
        context "uniqueness of" do 
            it { should validate_uniqueness_of(:beneficiary_application_id).with_message('has already financial information') }
        end
        context "enum values" do
            it { should define_enum_for(:residency_type).with(SakaniConstants::BeneficiaryApplicationFamilyResidencyInformation::RESIDENCY_TYPES) }
            it { should define_enum_for(:residency_unit_type).with(SakaniConstants::BeneficiaryApplicationFamilyResidencyInformation::RESIDENCY_UNIT_TYPES) }
            it { should define_enum_for(:residency_status).with(SakaniConstants::BeneficiaryApplicationFamilyResidencyInformation::RESIDENCY_STATUSES) }
        end
        context "belong to" do
            it { should belong_to(:beneficiary_application) }
        end
    end

end
