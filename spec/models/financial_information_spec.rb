require 'rails_helper'

RSpec.describe FinancialInformation, type: :model do
    describe "validations" do
        let(:beneficiary_application) { create(:beneficiary_application) }
        
        subject { build :financial_information, beneficiary_application: beneficiary_application }  

        context "Have valid factory" do
            it { should be_valid }
        end
        context "presence of" do 
            it { should validate_presence_of(:costs) }
            it { should validate_presence_of(:cost_description) }
            it { should validate_presence_of(:amount_of_down_payment) } 
            it { should validate_presence_of(:family_other_income) } 
            it { should validate_presence_of(:family_monthly_salary) } 
            it { should validate_presence_of(:beneficiary_application_id) } 
        end
        context "uniqueness of" do 
            it { should validate_uniqueness_of(:beneficiary_application_id).with_message('has already financial information') }
        end
        context "belong to" do
            it { should belong_to(:beneficiary_application) }
        end
    end

end