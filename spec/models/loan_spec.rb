require 'rails_helper'

RSpec.describe Loan, type: :model do
    describe "validations" do
        let(:beneficiary_application) { create(:beneficiary_application) }
        
        subject { build :loan, beneficiary_application: beneficiary_application }  

        context "Have valid factory" do
            it { should be_valid }
        end
        context "presence of" do 
            it { should validate_presence_of(:loan_type) }
            it { should validate_presence_of(:loan_provider_name) } 
            it { should validate_presence_of(:loan_amount) }
            it { should validate_presence_of(:remaining_loan_amount) } 
            it { should validate_presence_of(:monthly_loan_payment) } 
            it { should validate_presence_of(:loan_reason) } 
            it { should validate_presence_of(:beneficiary_application) } 
        end
        context "enum values" do
            it { should define_enum_for(:loan_type).with(SakaniConstants::BeneficiaryApplicationFinancialInformation::LOAN_TYPES) }
        end
        context "belong to" do
            it { should belong_to(:beneficiary_application) }
        end
    end

end
