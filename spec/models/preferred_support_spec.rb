require 'rails_helper'

RSpec.describe PreferredSupport, type: :model do
    describe "validations" do
        let(:beneficiary_application) { create(:beneficiary_application) }
        
        subject { build :preferred_support, beneficiary_application: beneficiary_application }  

        context "Have valid factory" do
            it { should be_valid }
        end
        context "presence of" do 
            it { should validate_presence_of(:beneficiary_application_id) } 
            it { should validate_presence_of(:city) } 
            it { should validate_presence_of(:region) } 
            it { should validate_presence_of(:first_choice) } 
            it { should validate_presence_of(:second_choice) } 
            it { should validate_presence_of(:third_choice) } 
            it { should validate_presence_of(:fourth_choice) } 
        end
        context "uniqueness of" do 
            it { should validate_uniqueness_of(:beneficiary_application).with_message('has already preferred support information') }
        end
        context "uniqueness of choices" do
            
            describe "first_choice" do
                it "cant be same as other choice" do
                    subject.first_choice = subject.second_choice
                    expect(subject).not_to be_valid
                end
            end

            describe "second_choice" do
                it "cant be same as other choice" do
                    subject.second_choice = subject.third_choice
                    expect(subject).not_to be_valid
                end
            end

            describe "third_choice" do
                it "cant be same as other choice" do
                    subject.third_choice = subject.second_choice
                    expect(subject).not_to be_valid
                end
            end

            describe "fourth_choice" do
                it "cant be same as other choice" do
                    subject.fourth_choice = subject.second_choice
                    expect(subject).not_to be_valid
                end
            end
        end
        context "belong to" do
            it { should belong_to(:beneficiary_application) }
        end
    end

end
