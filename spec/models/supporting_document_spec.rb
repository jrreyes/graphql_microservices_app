require 'rails_helper'

RSpec.describe SupportingDocument, type: :model do
    describe "validations" do
        let(:beneficiary_application) { create(:beneficiary_application) }
        
        subject { build :supporting_document, beneficiary_application: beneficiary_application }  

        context "Have valid factory" do
            it { should be_valid }
        end
        context "presence of" do 
            it { should validate_presence_of(:file) } 
        end
        context "belong to" do
            it { should belong_to(:beneficiary_application) }
        end
    end

end
